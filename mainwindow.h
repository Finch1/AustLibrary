#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPixmap>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkSession>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkCookie>
#include <QTextDocument>

namespace Ui {
class MainWindow;
}

struct Book{
    QString author;
    QString name;
    QString id;
    QString time_begin;
    QString time_end;
    QString url;
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
public slots:
    void requestFinished(QNetworkReply* reply);
private:
    Ui::MainWindow *ui;
    QNetworkAccessManager *manager;
public:
    static QString GetSubString(QString src, QString pattern);
    static QStringList GetSubStringEX(QString src, QString pattern);
    static QString Unescape(QString src);
    void SendRequest(QString url,QString data="");
    void RefreshBooksDisplay();
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_tabWidget_currentChanged(int index);

private:
    QString csrf;
    QString name;
    bool first = true;
    QList<Book> books;
};

#endif // MAINWINDOW_H
